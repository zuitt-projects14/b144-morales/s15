//Operator

/*

Add + = sum
Subtract- = Difference
Multilpy * = Product
Divide / = Quotient
Modulus % = Remainder

*/

function mod(){
	return 9 % 2;
}


console.log(mod());

// 8 * (6 + 2) - (2 * 3)

// Assignment Operator

/*

+= addition
-= Subtraction
*= Multiplication
/= Division
%= Modulo

*/

let x = 1;

let sum = 1;
/*let sum = sum + 1;*/
sum += 1;


console.log(sum);

//Increament and Decrement (++, --)
//Operators that add or subtract values by 1 and re assigns the values of the variables where the increment/decrement was applied to

let z = 1;
/*Pre-increment nag aad muna ng plus 1 value before aasigning to a new variable*/
let increment = ++z

console.log("Result of per-increment: " + increment);

console.log("Result of pre-increment: " + z);


/*Post-increment*/
increment = z++;
console.log("Result of post-increment: " + increment);

console.log("Result of post-increment: " + z);

//Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement)
console.log("Result of pre-decrement: " + z)

//Post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//Comparison Operators

//Equality Operator (==)
/*
let juan = "juan";

console.log(1 == 1);
console.log(0 == false);
console.log("juan" == juan)
//Strict Equality *(===)
console.log(1 === true);

//Inequality Operator (!=)
console.log("Inequality Operator");
console.log(1 != 1);
console.log("Juan" != juan);

//string inequality (!==)
console.log(0 !== false);
*/
//Other Equality Operator
/*
> = Greater than
< = Less than
>= Greater than or equal
<= Less than or equal
*/

//Logical operators
let isLegalAge = true;
let isRegistered = false;


/*
	And Operator (&&) - return true if all operands are true.
		true && true = true
		false && true = false
		true && false = false
	Or Operator (||) - return true if any of the operands or conditions are true
		true && true = true
		false && true = true
		true && false = true
*/

//And Operator
let allRequirementsMet = isLegalAge && isRegistered;
(console.log("Results of logical AND operator: " + allRequirementsMet))

//Or Operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection Control Structure
//if else statement

/* IF statement
		- execute a statement if a specific condition is true.
		Syntax
		if(condition){
			statement/s;
		}
*/
/*let num = -1;

if(num < 0){
	console.log("Hello");
}
*/
//activity


let num = 12;
if(num >= 10){
	console.log("Welcome to Zuitt")
}

/*IF-ELSE statement
	- execute a statement if the previous condition returns false.
	Syntax:
	if(condition){
		statement/s;
	}
	else {
		statement/s;
	}
*/

num = 5;
if(num >= 10){
	console.log("Number is greater or equal to 10");
}
else {
	console.log("Number is not greater or equal to 10");
}


//Mini activity

/*let age = parseInt(prompt("Please provide age: "));
if(age > 59){
	alert("Senior Age")
}
else {
	alert("Invalid Age")
}*/

// IF-ELSEIF-ELSE statement
/*
Syntax:
if(condition){
	statement/s
} else if(condition){
	statement/s
} else if (condition){
	statement/s
}
.
.
else{
	statement/s
}
.

	1 - Quezon City
	2 - Valenzuela
	3 - Pasig
	4 - Taguig

*/

/*let city = parseInt(prompt("Enter a number: "));
if(city === 1){
	alert("Welcome to Quezon City");
}
else if(city === 2){
	alert("Welcome to Valezuela City");
}
else if(city === 3){
	alert("Welcome to Pasig City");
}
else if(city === 4){
	alert("Welcome to Taguig City");
}
else {
	alert("Invalid Number")
}*/


let message = "";

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet";
	}
	else if(windSpeed <= 61){
		return "Tropical depression detected";
	}
	else if (windSpeed > 61 && windSpeed <= 88){
		return "Tropical storm detected.";
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical storm detected";
	}
	else {
		return "typhoon detected"
	}
}


message = determineTyphoonIntensity(70);
console.log(message);

//Ternary Operator
/*
	Syntax:
	(condition) ? ifTrue : ifFalse
*/

/*let ternaryResult = (1 < 18) ? "valid" : "invalid";
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name: "John";
	return "You are of the legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert("Result of Ternary Operator in function: " + legalAge + ", " + name);
*/
// Switch Statement
/*
	Syntax:
		switch(expression){
			case value1:
				statement/s;
					break;
			case value2:
			statement/s;
					break;
			case valueN: 
			statement/s;
					break;
			default: //parang si else
				statement/s;
			//no need to put break; since end na
		}
*/

//Switch statement example
//case sensitive
let day = prompt("What day of the week is it today?").toLowerCase();
switch (day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
		case "tuesday":
		alert("The color of the day is yellow");
		break;
		case "wednesday":
		alert("The color of the day is green");
		break;
		case "thursday":
		alert("The color of the day is blue");
		break;
		case "friday":
		alert("The color of the day is indigo");
		break;
		case "saturday":
		alert("The color of the day is violet");
		break;
		default:
			alert("Please input valid day");
}



//Try-Catch-Finally Statement - commonly used for error handling only

function showIntensityALert(windSpeed){
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}

showIntensityALert(56);









































































































